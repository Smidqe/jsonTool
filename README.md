Note:
	This requires the json-simple library that can be downloaded from
	https://code.google.com/archive/p/json-simple or
	https://github.com/fangyidong/json-simple/releases


This tool/lib is meant for turning the JSONObject that json-simple lib returns to a more managaeble format. 

Every section has subsections and values, every JSONObject is turned into a 
TSection while all values are turned into strings, sure you need to parse those 
strings to access the values and this is something that I will be fixing at some
point
