package parser.TSection;

import java.util.Map;
import java.util.WeakHashMap;

public class TSection<K, V> 
{
	//will hold all single keys
	private Map<K, V> __values;
	
	//this will hold all subsections if it has one
	private Map<K, TSection<K, V>> __subsections;
	
	private String type;
	private String name;
	
	public TSection() 
	{
		this.__subsections = new WeakHashMap<K, TSection<K, V>>();
		this.__values = new WeakHashMap<K, V>();
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void addSubsection(K name, TSection<K, V> __section)
	{
		this.__subsections.put(name, __section);
	}
	
	public void addValue(K key, V value)
	{
		this.__values.put(key, value);
	}
	
	public boolean hasSubsections()
	{
		return this.__subsections.size() > 0;
	}
	
	public Map<K, V> getValues()
	{
		return this.__values;
	}
	
	public Map<K, TSection<K, V>> getSubsections()
	{
		return this.__subsections;
	}
	
	public V getValue(K key)
	{
		return this.__values.get(key);
	}
	
	public String getType()
	{
		return this.type;
	}

	public String getName()
	{
		return this.name;
	}
	
	public void print(boolean recursive)
	{
		System.out.println("Name: " + this.name);
		
		if (this.__subsections.size() != 0)
		{
			System.out.println("Subsections: ");
			for (K s : this.__subsections.keySet())
				System.out.println(s);
			
			System.out.println("");
		}	

		if (this.__values.size() != 0)
		{
			System.out.println("Single values: ");
			for (K s : this.__values.keySet())
				System.out.println(s + ": " + this.__values.get(s));
		
			System.out.println("");
		}

		if (recursive)
			for (K s : this.__subsections.keySet())
				this.__subsections.get(s).print(recursive);
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.__subsections.size() == 0 && this.__values.size() == 0;
	}
}
