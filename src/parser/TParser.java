package parser;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import parser.TSection.TSection;


public class TParser 
{
	private JSONObject __object;
	private TSection<String, String> __data;
	
	public TParser() 
	{
		this.__data = new TSection<String, String>(); 
	}
	
	public TParser(URL url) throws IOException, ParseException
	{
		this();
		this.__object = (JSONObject) new JSONParser().parse(new InputStreamReader(url.openStream()));
		 
	}
	
	public TParser(JSONObject object)
	{
		this();
		this.__object = object;
	}
	
	public TSection<String, String> get()
	{
		return this.__data;
	}
	
	//parses the JSONObject from json-simple libraray into TSections, uses recursive methods since we don't know how far we have to go.
	private void parse(TSection<String, String> __subsection, JSONObject object, JSONArray array)
	{
		if (object == null && array == null)
			return;
		
		Iterator<?> __iterator = null;
		
		//get the iterator from the object or the array
		if (object != null)
			__iterator = object.keySet().iterator();
		else
			__iterator = array.iterator();
		
		//variable declarations
		Object o = null;
		TSection<String, String> __sub = null;
		int id = 0;
		int subid = 0;
		
		while (__iterator.hasNext())
		{
			o = __iterator.next();

			if (object != null)
			{
				//add a new subsection if we have encountered a object or array
				if (object.get(o) instanceof JSONObject || object.get(o) instanceof JSONArray)
				{
					__sub = new TSection<String, String>();
					__sub.setName(String.valueOf(o));
					__subsection.addSubsection(String.valueOf(o), __sub);
				}
				
				//if we had an object
				if (object.get(o) instanceof JSONObject)
				{
					__sub.setType("Object");
					parse(__sub, (JSONObject) object.get(o), null);
					continue;
				}
				
				//what about an array?
				if (object.get(o) instanceof JSONArray)
				{
					__sub.setType("Array");
					parse(__sub, null, (JSONArray) object.get(o));
					continue;
				}
				
				//if we haven't hit any if cases above, then add a single value
				__subsection.addValue(String.valueOf(o), String.valueOf(object.get(o)));
			}
			
			//handle the cases on arrays
			if (array != null)
			{
				if (o instanceof JSONObject)
				{
					__sub = new TSection<String, String>();
					__sub.setType("Object");
					__subsection.addSubsection(String.valueOf(id), __sub);
					
					parse(__sub, (JSONObject) o, null);
				}
				else
				{
					__subsection.addValue(String.valueOf(subid), String.valueOf(o));
					subid++;
				}
			}
			
			id++;
		}
	}
	
	public void print(boolean recursive)
	{
		if (this.__data.isEmpty())
			parse(this.__data, this.__object, null);
		
		this.__data.print(recursive);
	}
	
	public void parse()
	{
		parse(this.__data, this.__object, null);
	}
}
